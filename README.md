OpenShift WebConsole Examples
=============================

Brought to you by HCS Company. This should work when you're cluster-admin on an
OpenShift Container Platform version 4.x.


HTPasswd
========

`oc create secret generic htpasswd-secret --from-file=htpasswd -n openshift-config`

`oc create -f htpasswd.yml`

Custom Logo
===========

`oc create configmap hcs-ocp-logo --from-file hcs-ocp-logo.svg -n openshift-config`

`oc apply -f hcs-logo.yml`
